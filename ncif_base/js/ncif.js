(function ($, Drupal) {

  "use strict";

  // Drupal.behaviors.customBehavior = {
  //   // perform jQuery as normal in here
  // };

  function InitializeLoginStatus() {

    // Hides or shows the logout flyout based on active panel status
    $("div.FnlWelcomeMessage > span").on("click", function (event) {
      var welcomeMsg = $(this).parents("div.FnlWelcomeMessage");
      if (welcomeMsg.hasClass("Active")) {
        welcomeMsg.removeClass("Active");
      }
      else {
        welcomeMsg.addClass("Active");
      }
    });
  
    // Closes logout flyout if the click is outside of the panel
    $(document).on("mouseup", function (e) {
      var welcomeMsg = $("div.FnlWelcomeMessage");
      if (!welcomeMsg.is(e.target) && welcomeMsg.has(e.target).length == 0) {
        welcomeMsg.removeClass("Active");
      }
    });
  }

  InitializeLoginStatus();
    
})(jQuery);